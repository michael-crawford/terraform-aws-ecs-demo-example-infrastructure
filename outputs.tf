output "proxy_image_access_key_id" {
  value = aws_iam_access_key.proxy_image.id
}

output "proxy_image_secret_access_key" {
  value = aws_iam_access_key.proxy_image.secret
  sensitive = true
}

output "proxy_image_repository_url" {
  value = aws_ecr_repository.proxy_image.repository_url
}

output "api_image_access_key_id" {
  value = aws_iam_access_key.api_image.id
}

output "api_image_secret_access_key" {
  value = aws_iam_access_key.api_image.secret
  sensitive = true
}

output "api_image_repository_url" {
  value = aws_ecr_repository.api_image.repository_url
}
