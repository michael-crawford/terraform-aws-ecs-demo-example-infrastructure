variable "company" {
  description = "Company Name"
  type        = string
  default     = "MJCConsulting"
}

variable "region" {
  description = "AWS Region"
  type        = string
  default     = "us-east-2"
}

variable "environment" {
  description = "Environment Name"
  type        = string
  default     = "Production"
}

variable "application" {
  description = "Application Name"
  type        = string
  default     = "Recipe"
}

variable "proxy_component" {
  description = "Proxy Component Name"
  type        = string
  default     = "Proxy"
}

variable "api_component" {
  description = "API Component Name"
  type        = string
  default     = "API"
}

variable "project" {
  description = "Project Name"
  type        = string
  default     = "ECS Demo"
}

variable "domain" {
  description = "Domain Name"
  type        = string
  default     = "x.terraform1.mjcconsulting.com"
}

variable "proxy_image" {
  description = "Proxy Image Name"
  type        = string
  default     = "recipe-proxy"
}

variable "proxy_image_user" {
  description = "Proxy Image User Name"
  type        = string
  default     = "recipe-proxy-user"
}

variable "proxy_image_gitlab_project_id" {
  description = "Proxy Image GitLab Project Id"
  type        = string
  default     = "37982961"
}

variable "api_image" {
  description = "API Image Name"
  type        = string
  default     = "recipe-api"
}

variable "api_image_user" {
  description = "API Image User Name"
  type        = string
  default     = "recipe-api-user"
}

variable "api_image_gitlab_project_id" {
  description = "API Image GitLab Project Id"
  type        = string
  default     = "37983017"
}

variable "company_codes" {
  description = "Company Name to Code Map"
  type = map(string)
  default = {
    MJCConsulting = "mjc"
    CaMeLz        = "cml"
  }
}

variable "region_codes" {
  description = "Region Name to Code Map"
  type = map(string)
  default = {
    us-east-1 = "ue1"
    us-east-2 = "ue2"
    us-west-2 = "uw2"
    eu-west-1 = "ew1"
  }
}

variable "location_codes" {
  description = "Location Name to Code Map"
  type = map(string)
  default = {
    SantaBarbara = "sba"
    NewYork      = "nyc"
    LosAngeles   = "lax"
    Dallas       = "dfw"
  }
}

variable "account_codes" {
  description = "Account Name to Code Map"
  type = map(string)
  default = {
    Management  = "m"
    Log         = "l"
    Audit       = "a"
    Network     = "n"
    Core        = "c"
    Build       = "b"
    Sandbox     = "x"
    Production  = "p"
    Staging     = "s"
    Testing     = "t"
    Development = "d"
  }
}

variable "environment_codes" {
  description = "Environment Name to Code Map"
  type = map(string)
  default = {
    Management  = "m"
    Log         = "l"
    Audit       = "a"
    Network     = "n"
    Core        = "c"
    Build       = "b"
    Sandbox     = "x"
    Production  = "p"
    Staging     = "s"
    Testing     = "t"
    Development = "d"
  }
}

variable "application_codes" {
  description = "Application Name to Code Map"
  type = map(string)
  default = {
    Recipe  = "rec"
  }
}

variable "component_codes" {
  description = "Component Name to Code Map"
  type = map(string)
  default = {
    Proxy  = "p"
    API    = "a"
  }
}
