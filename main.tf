provider "aws" {
  region = var.region
}

locals {
  company_code = lookup(var.company_codes, var.company, "mjc")
  region_code = lookup(var.region_codes, var.region, "ue1")
  environment_code = lookup(var.environment_codes, var.environment, "x")

  common_tags = {
    Company     = var.company
    Environment = var.environment
    Project     = var.project
  }
}

########################################################################################################################
# Proxy Container Image
########################################################################################################################

resource "aws_iam_policy" "proxy_image" {
  name        = "${var.application}${var.proxy_component}ImagePublicationAccess"
  path        = "/"
  description = "Policy to publish ${var.application} ${var.proxy_component} Image to ECR"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "ecr:*"
        ]
        Resource = "arn:aws:ecr:${var.region}:*:repository/${var.proxy_image}"
      },
      {
        Effect = "Allow"
        Action = [
          "ecr:GetAuthorizationToken"
        ]
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_user" "proxy_image" {
  name = var.proxy_image_user
  path = "/gitlab/"

  tags = merge(
    { Application = var.application },
    { Component = var.proxy_component },
    local.common_tags
  )
}

resource "aws_iam_user_policy_attachment" "proxy_image" {
  user       = aws_iam_user.proxy_image.name
  policy_arn = aws_iam_policy.proxy_image.arn
}

resource "aws_iam_access_key" "proxy_image" {
  user    = aws_iam_user.proxy_image.name
}

resource "gitlab_project_variable" "proxy_image_access_key_id" {
  project   = var.proxy_image_gitlab_project_id
  key       = "AWS_ACCESS_KEY_ID"
  value     = aws_iam_access_key.proxy_image.id
  protected = true
  masked    = false
}

resource "gitlab_project_variable" "proxy_image_secret_access_key" {
  project   = var.proxy_image_gitlab_project_id
  key       = "AWS_SECRET_ACCESS_KEY"
  value     = aws_iam_access_key.proxy_image.secret
  protected = true
  masked    = true
}

resource "aws_ecr_repository" "proxy_image" {
  name                 = var.proxy_image
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "gitlab_project_variable" "proxy_image_ecr_repo" {
  project   = var.proxy_image_gitlab_project_id
  key       = "ECR_REPO"
  value     = aws_ecr_repository.proxy_image.repository_url
  protected = true
  masked    = false
}

########################################################################################################################
# API Container Image
########################################################################################################################

resource "aws_iam_policy" "api_image" {
  name        = "${var.application}${var.api_component}ImagePublicationAccess"
  path        = "/"
  description = "Policy to publish ${var.application} ${var.api_component} Image to ECR"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "ecr:*"
        ]
        Resource = "arn:aws:ecr:${var.region}:*:repository/${var.api_image}"
      },
      {
        Effect = "Allow"
        Action = [
          "ecr:GetAuthorizationToken"
        ]
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_user" "api_image" {
  name = var.api_image_user
  path = "/gitlab/"

  tags = merge(
    { Application = var.application },
    { Component = var.api_component },
    local.common_tags
  )
}

resource "aws_iam_user_policy_attachment" "api_image" {
  user       = aws_iam_user.api_image.name
  policy_arn = aws_iam_policy.api_image.arn
}

resource "aws_iam_access_key" "api_image" {
  user    = aws_iam_user.api_image.name
}

resource "gitlab_project_variable" "api_image_access_key_id" {
  project   = var.api_image_gitlab_project_id
  key       = "AWS_ACCESS_KEY_ID"
  value     = aws_iam_access_key.api_image.id
  protected = true
  masked    = false
}

resource "gitlab_project_variable" "api_image_secret_access_key" {
  project   = var.api_image_gitlab_project_id
  key       = "AWS_SECRET_ACCESS_KEY"
  value     = aws_iam_access_key.api_image.secret
  protected = true
  masked    = true
}

resource "aws_ecr_repository" "api_image" {
  name                 = var.api_image
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "gitlab_project_variable" "api_image_ecr_repo" {
  project   = var.api_image_gitlab_project_id
  key       = "ECR_REPO"
  value     = aws_ecr_repository.api_image.repository_url
  protected = true
  masked    = false
}
