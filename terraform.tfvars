company = "MJCConsulting"
region = "us-east-2"
environment = "Production"
application = "Recipe"
proxy_component = "Proxy"
api_component = "API"
project = "ECS Demo"
domain = "x.terraform1.mjcconsulting.com"
proxy_image = "recipe-proxy"
proxy_image_user = "recipe-proxy-user"
proxy_image_gitlab_project_id = "37982961"
api_image = "recipe-api"
api_image_user = "recipe-api-user"
api_image_gitlab_project_id = "37983017"
